class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]
  
  # GET /people
  # GET /people.json
  def index
    @people = Person.all
    @person = Person.new
    @peopleCache = PersonCache.all
    
  end
  def confirmationBox
    @closestUser = findClosestUser(@person)
  end
  # POST /people
  # POST /people.json
  def confirmation
    @person, newParams = createUserAndParams(params);
    # Parameters: {"utf8"=>"✓", "authenticity_token"=>"VJSUa47sAZxHmWPgRMm0IUSJ9hVKkSAP3JWlG8WIiO9vWJXgBS1O3Brpu3VI6mvi5sqjwx/jrZxQGI9noYnAeg==", 
    #"animal"=>"Dog", "height_in_feet"=>"6", "height_in_inches"=>"6.0", 
    #"isCache"=>"false", "isStorage"=>"false", "value"=>"Yes", "weight"=>"6.0"}
    respond_to do |format|
      if params[:value].to_s == "Yes" #Right guesss.
        @PersonCache = PersonCache.new(newParams)
        if params[:isCache].to_s =="false" and params[:isStorage].to_s =="true"# not in cache, but in storage, add it to cache
          if @PersonCache.save
            #puts 'This was not in cache, but in storage, soo... i saved to cache as well'
          end
        elsif params[:isCache].to_s == "false" && params[:isStorage].to_s =="false"#not in cache, not in storage, add to cache. 
          @person = createUserAndParams(@PersonCache)[0]
          if @person.save
            puts "********was in neither but now its in both"
          end
          if @PersonCache.save
            #puts " this was neither cache, nor storage, but it is now in Cache"
          end
        end
        format.html { redirect_to people_url , notice: 'I knew it'}
      else # Response is no, so update the animal and proceed.
        @PersonCache = PersonCache.new(newParams)
        if params[:isCache].to_s =="true" && params[:isStorage].to_s =="false" #Update it in the cache
          currentUser = PersonCache.where("weight=?",params[:weight]).where("height_in_inches=?",params[:height_in_inches]).where("height_in_feet=?",params[:height_in_feet]).first
          currentUser = changeAnimal(currentUser)
          if currentUser.save
            #puts " it was in cache with wrong animal, so it is updated  now..."
          end
        end
        if params[:isStorage].to_s == "true"
          #Update storage
          currentUserStorage =Person.where("weight=?",params[:weight]).where("height_in_inches=?",params[:height_in_inches]).where("height_in_feet=?",params[:height_in_feet]).first
          currentUserStorage = changeAnimal(currentUserStorage)
          if currentUserStorage.save
            #puts "it is updated in storage now"
          end
          newParams['animal'] = currentUserStorage['animal']
          @PersonCache = PersonCache.new(newParams)
          # add it to cache now
          if @PersonCache.save
            #puts "it was in storage, with different animal, so updated it there and added to cache"
          end
        end
        if params[:isCache].to_s =="false" && params[:isStorage].to_s =="false" #it is in neither cache nor storage, so save it to cache with new animal.
          @PersonCache = changeAnimal(@PersonCache)

          @person = createUserAndParams(@PersonCache)[0]
          if @person.save
            puts "saved to cache now as it is"
          end
          if @PersonCache.save
            puts "Was not in cache, and not in storage, and my guess was wrong. Saved in cache with a different name"
          end
        end
        format.html { redirect_to people_url , notice: 'Sorry, but I will remember that'}
      end
    end
  end
  def create
    @person = Person.new(person_params)
    @people = PersonCache.new(person_params)
    @closestUser = findClosestUser(@person)
    #confirmationBox
    respond_to do |format|
      format.html { render :confirmationBox,location: @person }
      puts @closestUser
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:height_in_inches, :height_in_feet, :weight, :animal)
    end
    def cacheParams(param)
      param.permit(:height_in_inches, :height_in_feet, :weight, :animal)
    end

    def closest3Or11(arrOfHeightWeight, currentHeightWeight)
        return nil if arrOfHeightWeight.size == 0 || currentHeightWeight.nil?
        arrOfHeightWeight = arrOfHeightWeight.to_a.map(&:serializable_hash)
        arrOfHeightWeight.each do | individualObject |
          individualObjectFullHeight = (individualObject['height_in_feet'] * 12).to_i + individualObject['height_in_inches'].to_d
          currentHeightWeightFullHeight = (currentHeightWeight['height_in_feet']* 12).to_i  + currentHeightWeight['height_in_inches'].to_d
          distance = (individualObjectFullHeight - currentHeightWeightFullHeight) ** 2;
          distance+= (individualObject['weight'] - currentHeightWeight['weight'])**2
          individualObject[:distance] = Math.sqrt(distance)
        end
       arrOfHeightWeight.sort_by { |obj| obj[:distance]}
       arrOfHeightWeight.take(3) if arrOfHeightWeight.size <= 10
       arrOfHeightWeight.take(11) if arrOfHeightWeight.size > 10
    end
    #Return the maximum of cat/dog among 3 or 11 closest points. 
    def max(closest3Or11)
    cat = 0
    dog = 0;
    if closest3Or11.nil?
      return  ['Dog','Cat'][(rand * 2).to_i]
    end
    closest3Or11.each do |eachObject|
      eachObject['animal'] =='Cat'? cat+=1 : dog+=1
    end

    if cat > dog
       return "Cat"
    elsif dog > cat
      return "Dog"
    else
      return  ['Dog','Cat'][(rand * 2).to_i]
    end
  end

    # return  a user closest to person in 2D spacial plane and where
    # the user was found(cache or storage)
    def findClosestUser(inCache=false, inStorage=false,person)
      @person = Person.new(person_params)
      cacheUser = PersonCache.where("weight = ?", person_params['weight']).where("height_in_feet = ?", person_params['height_in_feet']).first 
      if cacheUser && !cacheUser.nil? #User is found in cache, so just set isCache and return it. 
        inCache = true;
        return cacheUser,inCache,inStorage
      else #####check if the user is in the training data table(Person table)
        cacheUser = Person.where("weight = ?", person_params['weight']).where("height_in_feet = ?", person_params['height_in_feet']).first 
        if cacheUser && !cacheUser.nil? #User is found in training data, se the user, and isStorage and return them

          newParams = Hash.new
          newParams['weight'] = cacheUser.read_attribute('weight')
          newParams['height_in_inches'] = cacheUser.read_attribute('height_in_inches')
          newParams['height_in_feet'] = cacheUser.read_attribute('height_in_feet')
          newParams['animal'] = cacheUser.read_attribute('animal') 
          cacheParams = ActionController::Parameters.new(newParams)
          cacheParams = cacheParams(cacheParams)
          personFromTraining = PersonCache.new(cacheParams)
          inStorage = true
          return personFromTraining,inCache, inStorage
        else ## User is neither in training data nor cache data, so try guessing based on training data(majority of 3 or 11 closes points in 2D grph of weight and height)
          #puts 'Neither training nor storage, so find it now.... '
          @ListOfPeople = Person.all
          if @ListOfPeople.nil?
            return nil
          end
          closest3Or11 = closest3Or11(@ListOfPeople,person)
          animal = max(closest3Or11)
          person[:animal] = animal
          return person,inCache,inStorage
          end
      end#end first if. 
    end
    def changeAnimal(inputhash)
      if(inputhash['animal'].to_s =="Dog")
        inputhash['animal'] = "Cat"
      else
        inputhash['animal'] ="Dog"
      end
      return inputhash
    end
    def createUserAndParams(cache)
      newParams = Hash.new
      newParams['weight'] = cache[:weight]
      newParams['height_in_inches'] = cache[:height_in_inches]
      newParams['height_in_feet'] = cache[:height_in_feet]
      newParams['animal'] = cache[:animal]
      @person = Person.new(newParams)
      return @person, newParams
    end
end
