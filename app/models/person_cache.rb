class PersonCache < ApplicationRecord
    validates :height_in_inches, :height_in_feet, :weight, :animal, presence: true
    validates :height_in_inches, numericality: {greater_than: 0, less_than_or_equal_to: 10}
    validates :weight, numericality: {greater_than: 0, less_than_or_equal_to: 600} 
    validates :height_in_feet, numericality: {greater_than: 0, less_than_or_equal_to: 9}
end
