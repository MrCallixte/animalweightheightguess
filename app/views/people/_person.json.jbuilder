json.extract! person, :id, :height_in_inches, :height_in_feet, :weight, :animal, :created_at, :updated_at
json.url person_url(person, format: :json)
