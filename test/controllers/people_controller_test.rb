require 'test_helper'

class PeopleControllerTest < ActionDispatch::IntegrationTest
  setup do
    @person = people(:one)
  end
  test "Visits home page" do
    visits people_url
    assert_select "form"
    assert_select "form input", 3
  end

  test "should get index" do
    get people_url
    expect(response).to be_successful
  end

  test "should create person" do
    assert_difference('Person.count') do
      post "people#confirmation", params: { person: { animal: @person.animal, height_in_feet: @person.height_in_feet, height_in_inches: @person.height_in_inches, weight: @person.weight } }
    end
    assert_redirected_to person_url(Person.last)
  end

end
