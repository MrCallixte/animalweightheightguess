require 'rails_helper'

RSpec.describe Person, type: :model do
  context 'validatation tests'  do
    it 'ensures weight presence' do
      person = Person.new(height_in_feet: 4, height_in_inches: 4, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures weight > 0 lbs' do
      person = Person.new(weight: -4, height_in_feet: 4, height_in_inches: 4, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures weight < 600 lbs' do
      person = Person.new(weight: 700, height_in_feet: 4, height_in_inches: 4, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures height_in_feet presence' do
      person = Person.new(weight: 10, height_in_inches: 4, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures height_in_feet >= 0 ft' do
      person = Person.new(weight: 10, height_in_inches: 4, height_in_feet: -1, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures height_in_feet <= 9 ft' do
      person = Person.new(weight: 10, height_in_inches: 4, height_in_feet: 11, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures height_in_inches presence' do
      person = Person.new(weight: 10, height_in_feet: 4, animal: 'Dog').save
      expect(person).to eq(false)
    end

    it 'ensures height_in_inches >=0 in' do
      person = Person.new(weight: 10, height_in_feet: 4, height_in_inches: -3, animal: 'Dog').save
      expect(person).to eq(false)
    end
    it 'ensures height_in_inches <12 in' do
      person = Person.new(weight: 10, height_in_feet: 4, height_in_inches: 12, animal: 'Dog').save
      expect(person).to eq(false)
    end

    it 'ensures animal presence' do
      person = Person.new(weight: 10, height_in_feet: 4, height_in_inches: 4).save
      expect(person).to eq(false)
    end
    it 'should save successfully if all provided fields are in range' do
      person = Person.new(weight: 10, height_in_feet: 4, height_in_inches: 4, animal: 'Dog').save
      expect(person).to eq(true)
    end
  end
  context 'scope tests' do
  end
end
