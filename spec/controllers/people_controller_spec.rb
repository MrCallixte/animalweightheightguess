require 'rails_helper'

RSpec.describe PeopleController, type: :controller do
    context 'get #index' do
        it 'returns success response for index page' do
            get :index
            expect(response).to have_http_status(:successful)
        end
    end
    context 'post #create' do
        it 'Should guess my favorite animal' do
            post :create, params: { person:params = {"height_in_feet"=>"4","height_in_inches"=>"4","weight"=>"4"} }
            expect(response).to have_http_status(:successful)
        end
        it 'Should guess my favorite animal' do
            post :create, params: { person:params = {"height_in_inches"=>"4","weight"=>"4"} }
            expect(response).to have_http_status(:successful)
        end
        it 'Should redirect to confirmation page of age' do
            post :create, params: { person:params = {"height_in_feet"=>"4","height_in_inches"=>"4","weight"=>"4"} }
            expect(response).to render_template(:confirmationBox)
        end 
    end
    context 'post #confirmation' do
        it 'should respond with redirect code 302' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"false", "isStorage"=>"true", 
                "value"=>"Yes", "weight"=>"4.0"
            }}
            expect(response.code).to eq "302"
        end
        it 'should respond with redirect code 302' do
            post :confirmation, params: {PersonCache:params = {}}
            expect(response.code).to eq "302"
        end
        it 'should respond with redirect code 302' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"false", "isStorage"=>"true", 
                "value"=>"No", "weight"=>"4.0"
            }}
            expect(response.code).to eq "302"
        end
        it 'expect respond with redirect code 302' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"false", "isStorage"=>"false", 
                "value"=>"Yes", "weight"=>"4.0"
            }}
            expect(response.code).to eq "302"
        end
        it 'should respond with redirect code 302' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"false", "isStorage"=>"false", 
                "value"=>"No", "weight"=>"4.0"
            }}
            expect(response.code).to eq "302"
        end
        it 'should respond with redirect code 302' do
            post :confirmation, params: {newParams:params = {"animal"=>"Dog","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"false", "isStorage"=>"false", 
                "value"=>"Yes", "weight"=>"4.0"
            }}
            expect(response.code).to eq "302"
        end
        it 'should redirect to index page when confirmation is yes' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"true", "isStorage"=>"false", 
                "value"=>"Yes", "weight"=>"4.0"
            }}
            expect(response).to redirect_to(people_url)
        end
        it 'should redirect to index page when confirmation is yes' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"false", "isStorage"=>"false", 
                "value"=>"Yes", "weight"=>"4.0"
            }}
            expect(response).to redirect_to(people_url)
        end
        it 'should redirect to index page when confirmation is No' do
            post :confirmation, params: {newParams:params = {"animal"=>"Cat","height_in_feet"=>"4",
                "height_in_inches"=>"4.0","isCache"=>"true", "isStorage"=>"false", 
                "value"=>"Yes", "weight"=>"4.0"
            }}
            expect(response).to redirect_to(people_url)
        end
    end
    

end
