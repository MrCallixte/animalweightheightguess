Rails.application.routes.draw do
  root 'people#index'
  resources :people
  post "people_controller/confirmation", :to => "people#confirmation"

end
