#### This is a seeding script that will load all training data at the beginning

require 'csv'
puts " There are currently #{Person.count} rows in the Person table, clearing......"
Person.destroy_all
csv_text = File.read(Rails.root.join('config','data','AnimalHeightWeight.csv'))
puts csv_text
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
    person = Person.new
    person.weight = row['Weight']
    person.height_in_feet = row['Height'].to_i / 12;
    person.height_in_inches = row['Height'].to_d - 12 * person.height_in_feet.to_d
    person.animal = row['Animal']
    person.save
end
puts "#{Person.count} rows just got added to the Person table"