class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.decimal :height_in_inches
      t.integer :height_in_feet
      t.decimal :weight
      t.string :animal

      t.timestamps
    end
  end
end
