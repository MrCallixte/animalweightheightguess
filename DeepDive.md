# Algorithm Deep Dive

## Requirements

Guess the user’s preferred animal based on height and weight. 
`Input:` Sample model data(optional), user's weight in lbs( 0 - 600), user's height in inches(0-11), and user's height in feet(0-9)

## Assumptions: 

Users preferred animal can change later during the application execution. 

## Approach

First of all, the application has granularizes height for ease of input and computation. The algorithm is divided into 2 scenarios based on the user inputs.

1.	The height and weight of the user already exists in the application
2.	The height and weight of the user doesn’t exist

If the user's height and weight has been previously seen, simply confirm with the user about the preferred animal. If this is correct, then no much computation needed.  

If the application doesn’t have the user height and weight, then the application has to guess using the available data if there is enough data.  The approach used is to simulate people into a 2D plane where x and y are weight/ height(height/weight) and find the k closest people to the person the application is trying to guess for the preferred animal. Simply count the majority of any of the two animals and return that. Check with the user if the animal matches their preferred animal. If yes, save the actual data, if not, change the animal and save it. 

In order to make things easier, I made two database tables. Table that contains previously confirmed user’s guess(cache) and table that contains all the training data(storage)

In reality, cache would be in memory database that has a fast look up to avoid constant fetching the database. 

## Possible Scenarios that were followed: 

* User is only in cache, and the guess is correct, save the user in storage
* User is only in cache, and the guess is incorrect, update the cache, and save to storage 
* User is only in storage and the guess is correct, simply add the user in the cache
* User is only in storage and the guess is incorrect, simply add the user with correct data in the cache, and update the user in storage
* User is neither storage nor cache, and the guess is correct, simply save the user in both tables.
* User is neither in storage nor cache, and the guess is incorrect, simply update the animal, and save the user in both tables. 



  




