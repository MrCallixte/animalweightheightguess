# README

## Key points to run the application: 

Please install all the gems first and load initial training data. 

To install all dependencies 

```bundle  install```

 To create tables do

 ```rails db:migrate```
 
 The training data is located at 

```/config/data```

 You can use your own training data of choice. Please add an excell file that has the following headers
  ````Animal,Height,Weight````
  and make sure to update the seed file located at: `db/seed.rb`. Just replace `'AnimalHeightWeight.csv'` with your filename of choice on the  the line `csv_text = File.read(Rails.root.join('config','data','AnimalHeightWeight.csv'))`. 

Please keep in mind the the size of the file will impact the performance of the application. To get started, Currently, there are two files located at `config/data` namely:`'AnimalHeightWeight.csv'` and `'AnimalHeightWeight1.csv' `. `'AnimalHeightWeight1.csv'` has only about 34 rows and `'AnimalHeightWeight.csv'` has ~9K rows.
To load this file into database, run `rails db:seed`

To run the application simply do 

`rails s` 

and the application should start on port 3000

```http://localhost:3000```

To run the tests do

`rspec`
and coverage file is at [coverage stat](http://127.0.0.1:5500/coverage) or simply in the folder
```/coverage```

You can find the deep dive file at /DeepDive.md
